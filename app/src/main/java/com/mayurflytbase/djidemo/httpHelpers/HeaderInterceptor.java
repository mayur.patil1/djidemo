package com.mayurflytbase.djidemo.httpHelpers;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder builder = request.newBuilder();
        builder.header("User-Agent", "FLYTBASE_DJI_ANDROID");
        request = builder.build();
        return chain.proceed(request);
    }
}
