package com.mayurflytbase.djidemo;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mayurflytbase.djidemo.httpHelpers.HeaderInterceptor;
import com.mayurflytbase.djidemo.httpHelpers.WebSocketClient;
import com.mayurflytbase.djidemo.rosHelpers.FlytAPICallbacks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import dji.common.battery.BatteryState;
import dji.common.camera.SettingsDefinitions;
import dji.common.error.DJIError;
import dji.common.error.DJISDKError;
import dji.common.flightcontroller.FlightControllerState;
import dji.common.flightcontroller.FlightMode;
import dji.common.flightcontroller.virtualstick.FlightControlData;
import dji.common.flightcontroller.virtualstick.FlightCoordinateSystem;
import dji.common.flightcontroller.virtualstick.RollPitchControlMode;
import dji.common.flightcontroller.virtualstick.VerticalControlMode;
import dji.common.flightcontroller.virtualstick.YawControlMode;
import dji.common.mission.waypoint.Waypoint;
import dji.common.mission.waypoint.WaypointAction;
import dji.common.mission.waypoint.WaypointActionType;
import dji.common.mission.waypoint.WaypointMission;
import dji.common.mission.waypoint.WaypointMissionDownloadEvent;
import dji.common.mission.waypoint.WaypointMissionExecutionEvent;
import dji.common.mission.waypoint.WaypointMissionFinishedAction;
import dji.common.mission.waypoint.WaypointMissionFlightPathMode;
import dji.common.mission.waypoint.WaypointMissionGotoWaypointMode;
import dji.common.mission.waypoint.WaypointMissionHeadingMode;
import dji.common.mission.waypoint.WaypointMissionState;
import dji.common.mission.waypoint.WaypointMissionUploadEvent;
import dji.common.util.CommonCallbacks;
import dji.sdk.base.BaseComponent;
import dji.sdk.base.BaseProduct;
import dji.sdk.battery.Battery;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.mission.waypoint.WaypointMissionOperator;
import dji.sdk.mission.waypoint.WaypointMissionOperatorListener;
import dji.sdk.products.Aircraft;
import dji.sdk.sdkmanager.DJISDKInitEvent;
import dji.sdk.sdkmanager.DJISDKManager;
import edu.wpi.rail.jrosbridge.Ros;
import edu.wpi.rail.jrosbridge.Service;
import edu.wpi.rail.jrosbridge.Topic;
import edu.wpi.rail.jrosbridge.messages.Message;
import edu.wpi.rail.jrosbridge.services.ServiceRequest;
import edu.wpi.rail.jrosbridge.services.ServiceResponse;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static dji.common.mission.waypoint.WaypointMission.MAX_AUTO_FLIGHT_SPEED;
import static dji.common.mission.waypoint.WaypointMission.MAX_FLIGHT_SPEED;
import static java.lang.Double.isNaN;
import static java.lang.Math.max;
import static java.lang.Math.min;

public class MainActivity extends AppCompatActivity implements GoogleMap.OnMapClickListener, OnMapReadyCallback, FlytAPICallbacks {

    //private Aircraft aircraftInst;
    public static FlightController mFlightController;
    private Button btnTakeOff;
    private Button btnLand;
    private Button btnLeft;
    private Button btnRight;
    private Button btnUp;
    private Button btnDown;
    private Button btnPos;
    private Button btnAdd;
    private Button btnUpload;
    private Button btnStart;
    private Button btnStop;
    private Button btnLogin;
    private Button btnSid;

    private boolean isAdd = false;
    private final Map<Integer, Marker> mMarkers = new ConcurrentHashMap<Integer, Marker>();

    private GoogleMap gMap;

    private double droneLocationLat = 181, droneLocationLng = 181;
    private Marker droneMarker = null;

    private float altitude = 10.0f;
    private float mSpeed = 10.0f;

    private List<Waypoint> waypointList = new ArrayList<>();

    public static WaypointMission.Builder waypointMissionBuilder;

    private WaypointMissionOperator instance;
    private WaypointMissionFinishedAction mFinishedAction = WaypointMissionFinishedAction.NO_ACTION;
    private WaypointMissionHeadingMode mHeadingMode = WaypointMissionHeadingMode.AUTO;

    private final Handler publish5Hz_handler = new Handler();
    private final Handler publish2Hz_handler = new Handler();
    private final Handler ping_handler = new Handler();


    private String mode;
    private FlightMode fMode;
    private float GPSaltitude;
    private int sateliteCount;
    private float velocityX;
    private float velocityY;
    private float velocityZ;
    private double homeLatitude;
    private double homeAltitude;
    private double homeLongitude;
    private double latitude;
    private double longitude;

    private boolean isFlying;
    private boolean areMotorsOn;

    private final Handler handler = new Handler(Looper.getMainLooper());

    //battery
    private float mVoltage;
    private int mCharge;
    private int mBatteryPercentage, mRCBatteryPercentage;
    private int mCurrent;
    private int mBatteryCapacity;
    private final BatteryState.Callback DJIBatteryStateCallback = djiBatteryState -> {
        if (null == djiBatteryState) return;
        mVoltage = djiBatteryState.getVoltage() * 0.001f;
        mCharge = djiBatteryState.getChargeRemaining();
        mBatteryPercentage = djiBatteryState.getChargeRemainingInPercent();
        mCurrent = djiBatteryState.getCurrent();
        mBatteryCapacity = djiBatteryState.getFullChargeCapacity();
    };


    private WebSocketClient.OnMessageListener wsMsgListener = new WebSocketClient.OnMessageListener() {
        @Override
        public void onMessage(String data) {
//            logger.debug(wsMsgListener.hashCode() + data);
            if (data != null && data.equalsIgnoreCase("PING")) {
                return;
            }
            JSONParser.parseMessage(data, MainActivity.this);
        }
    };

    private final DJISDKManager.SDKManagerCallback registrationCallback = new DJISDKManager.SDKManagerCallback() {

        @Override
        public void onRegister(DJIError djiError) {
            //isRegistrationInProgress.set(false);
            if (djiError == DJISDKError.REGISTRATION_SUCCESS) {
                Log.d("App registration", DJISDKError.REGISTRATION_SUCCESS.getDescription());
                DJISDKManager.getInstance().startConnectionToProduct();
                showToast("registration success");
            } else {
                Log.e("DJI Reg failed ", djiError.getDescription());
            }
        }

        @Override
        public void onProductDisconnect() {
            Log.d("onProductDisconnect", "inside \"onProductDisconnect\"");
            showToast("product disconnect");
            //setResultToToast("DJI Drone Disconnected");
            //notifyDroneStatusChange();
        }

        @Override
        public void onProductConnect(BaseProduct baseProduct) {
            Log.d("onProductDisconnect", String.format("onProductConnect newProduct:%s", baseProduct));
            showToast("Product Connected");
            initDji();
        }

        @Override
        public void onComponentChange(BaseProduct.ComponentKey componentKey, BaseComponent oldComponent,
                                      BaseComponent newComponent) {
            Log.d("onComponentChange", "inside \"onComponentChange\"");
            if (newComponent != null) {
                newComponent.setComponentListener(new BaseComponent.ComponentListener() {

                    @Override
                    public void onConnectivityChange(boolean isConnected) {
                        Log.d("TAG", "onComponentConnectivityChanged: " + isConnected);
                        //initDji();
                    }
                });
            }

            Log.d("TAG",
                    String.format("onComponentChange key:%s, oldComponent:%s, newComponent:%s",
                            componentKey,
                            oldComponent,
                            newComponent));
            showToast("Component Change");

        }

        @Override
        public void onInitProcess(DJISDKInitEvent djisdkInitEvent, int i) {

        }

        @Override
        public void onDatabaseDownloadProgress(long l, long l1) {

        }
    };

    private final FlightControllerState.Callback DJIFlightstateCallback = djiFlightState -> {
        mode = djiFlightState.getFlightModeString();
        fMode = djiFlightState.getFlightMode();
        droneLocationLat = djiFlightState.getAircraftLocation().getLatitude();
        droneLocationLng = djiFlightState.getAircraftLocation().getLongitude();
        GPSaltitude = djiFlightState.getAircraftLocation().getAltitude();
        if (djiFlightState.isUltrasonicBeingUsed() && !djiFlightState.doesUltrasonicHaveError()) {
            altitude = djiFlightState.getUltrasonicHeightInMeters();
            if (altitude > 10.0) {
                altitude = GPSaltitude;
            }
        } else {
            altitude = GPSaltitude;
        }

        velocityX = djiFlightState.getVelocityX();
        velocityY = djiFlightState.getVelocityY();
        velocityZ = djiFlightState.getVelocityZ();
        sateliteCount = djiFlightState.getSatelliteCount();
        homeLatitude = djiFlightState.getHomeLocation().getLatitude();
        homeLongitude = djiFlightState.getHomeLocation().getLongitude();
        homeAltitude = 0.0;

        latitude = djiFlightState.getAircraftLocation().getLatitude();
        longitude = djiFlightState.getAircraftLocation().getLongitude();
        GPSaltitude = djiFlightState.getAircraftLocation().getAltitude();

        Roll = Math.toRadians(djiFlightState.getAttitude().roll);
        Pitch = Math.toRadians(djiFlightState.getAttitude().pitch);
        Yaw = Math.toRadians(djiFlightState.getAttitude().yaw);

        isFlying = djiFlightState.isFlying();
        areMotorsOn = djiFlightState.areMotorsOn();

        sateliteCount = djiFlightState.getSatelliteCount();
        updateDroneLocation();
    };

    private static Timer mSendVirtualStickDataTimer;
    private static SendVirtualStickDataTask mSendVirtualStickDataTask;
    private Ros ros;
    private String token;
    private String hardwareId;
    private String serial_number;
    private String guid;
    private String vehicleID;
    private WebSocketClient socket;

    private final Runnable publish5Hz_runnable = new Runnable() {
        @Override
        public void run() {
            publish5Hz();
            publish5Hz_handler.postDelayed(publish5Hz_runnable, 200);
        }
    };

    private final Runnable publish2Hz_runnable = new Runnable() {
        @Override
        public void run() {
            publish2Hz();
            publish2Hz_handler.postDelayed(publish2Hz_runnable, 500);
        }
    };
    private float mYaw;
    private float mPitch;
    private float mRoll;
    private float mThrottle;
    private FlightCoordinateSystem mCoordinateSystem;

    //used for IMU
    private double Roll = 0;
    private double Pitch = 0;
    private double Yaw = 0;

    private final Runnable ping_runnable = new Runnable() {
        @Override
        public void run() {
            callPingAPI();
            ping_handler.postDelayed(ping_runnable, 5000);
        }
    };
    private final boolean isGuided = false;


    private void callPingAPI() {
        /*if (null == mFlightController || null == guid || guid.isEmpty() || null == serial_number || serial_number.isEmpty())
            return;
*/
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HeaderInterceptor())
                .build();
        long epochSeconds = System.currentTimeMillis() / 1000L;
        String payload = MainApplication.getMyFlytbasePayloadHash(guid, epochSeconds, serial_number);
        JSONObject postBody = new JSONObject();
        try {
            postBody.put("id", guid);
            postBody.put("ts", String.valueOf(epochSeconds));
            postBody.put("sh", payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(JSON, postBody.toString());
        Request request = new Request.Builder()
                .url("https://my.flytbase.com/api/devices/ping/")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                //ToastUtils.setResultToToast("Ping Failed");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                //showLogMessage("Success PING API:--" + response.body().string());
                //ToastUtils.setResultToToast("Ping Success");
                Log.d("callPingApi resp: ", response.message().toLowerCase());
                //showToast("ping success"+ response);
            }
        });
    }


    //update drone location on map
    private void updateDroneLocation() {

        LatLng pos = new LatLng(droneLocationLat, droneLocationLng);
        //Create MarkerOptions object
        final MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(pos);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (droneMarker != null) {
                    droneMarker.remove();
                }

                if (checkGpsCoordinates(droneLocationLat, droneLocationLng)) {
                    droneMarker = gMap.addMarker(markerOptions);
                }
            }
        });
    }

    public static boolean checkGpsCoordinates(double latitude, double longitude) {
        return (latitude > -90 && latitude < 90 && longitude > -180 && longitude < 180) && (latitude != 0f && longitude != 0f);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // When the compile and target version is higher than 22, please request the
        // following permissions at runtime to ensure the
        // SDK work well.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.VIBRATE,
                            Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.WAKE_LOCK, Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SYSTEM_ALERT_WINDOW,
                            Manifest.permission.READ_PHONE_STATE,
                    }
                    , 1);
        }
        setContentView(R.layout.activity_main);

        startSDKRegistration();
        initView();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        addListener();
        ping_handler.postDelayed(ping_runnable, 1000);


        //rosInitialize();

    }

    /*private void rosCloudInitialize() {
        ros = new Ros("dev.flytbase.com", 9000, "", JRosbridge.WebSocketType.wss);

        Map<String, String> headersMap = new HashMap<>();
        headersMap.put("dev_guid", guid);
        headersMap.put("Content-Type", "application/json");
        ros.connect(headersMap);
        ros.addRosConnectionCallback(new ConnectionCallback() {

            @Override
            public void onOpen(org.java_websocket.handshake.ServerHandshake handshakedata) {
                Log.d("OnOpen", "connection open ");
                showToast("OnOpen - connection open");

                RosLib rosLib = new RosLib();
                //rosLib.advertiseDashTopics();
                rosLib.advertiseDashboardServices(MainActivity.this);
                showToast("advertising done");
                //startPublishingToRos();
            }

            @Override
            public void onClose(int code, java.lang.String reason, boolean remote) {
                Log.d("OnClose", "connection close " + reason);
                showToast("OnClose - connection close - " + reason);
            }

            @Override
            public void onError(Exception ex) {
                showToast("OnClose - connection error - " + ex.getMessage());
                ex.printStackTrace();
            }
        });

    }*/

    @Override
    public void onMapClick(LatLng latLng) {
        if (isAdd == true) {
            markWaypoint(latLng);
            showToast("marked waypoint at " + latLng.toString());
            Waypoint mWaypoint = new Waypoint(latLng.latitude, latLng.longitude, altitude);
            //Add Waypoints to Waypoint arraylist;
            if (waypointMissionBuilder != null) {
                waypointList.add(mWaypoint);
                waypointMissionBuilder.waypointList(waypointList).waypointCount(waypointList.size());
            } else {
                waypointMissionBuilder = new WaypointMission.Builder();
                waypointList.add(mWaypoint);
                waypointMissionBuilder.waypointList(waypointList).waypointCount(waypointList.size());
            }
        } else {
            showToast("Cannot add waypoint");
        }
    }


    //isolated dji sdk demo use
    private void uploadWayPointMission() {

        getWaypointMissionOperator().uploadMission(new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError error) {
                if (error == null) {
                    showToast("Mission upload successfully!");
                } else {
                    showToast("Mission upload failed, error: " + error.getDescription() + " retrying...");
                    getWaypointMissionOperator().retryUploadMission(null);
                }
            }
        });

    }


    private void markWaypoint(LatLng point) {
        //Create MarkerOptions object
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(point);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        Marker marker = gMap.addMarker(markerOptions);
        mMarkers.put(mMarkers.size(), marker);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Initializing Amap object
        if (gMap == null) {
            gMap = googleMap;
            setUpMap();
        }

        LatLng shenzhen = new LatLng(22.5362, 113.9454);
        gMap.addMarker(new MarkerOptions().position(shenzhen).title("Marker in Shenzhen"));
        gMap.moveCamera(CameraUpdateFactory.newLatLng(shenzhen));
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void setUpMap() {
        gMap.setOnMapClickListener(this);// add the listener for click for amap object
    }

    private void initView() {

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login("mayur.patil@flytbase.com", "machem@307");
            }
        });

        btnSid = (Button) findViewById(R.id.btnHwId);
        btnSid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchDroneHwId();
            }
        });


        btnStart = (Button) findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startWaypointMission();
            }
        });

        btnStop = (Button) findViewById(R.id.btnStop);
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopWaypointMission();
            }
        });


        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAdd == false) {
                    isAdd = true;
                    btnAdd.setText("Exit");
                } else {
                    isAdd = false;
                    btnAdd.setText("Add");
                }
            }
        });


        btnUpload = (Button) findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configWayPointMission();
                uploadWayPointMission();
            }
        });

        btnTakeOff = (Button) findViewById(R.id.btnTakeOff);
        btnTakeOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFlightController.startTakeoff(djiError -> {
                    showToast("TakeOff initiated");
                });
            }
        });


        btnLand = (Button) findViewById(R.id.btnLand);
        btnLand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFlightController.startLanding(djiError -> {
                    showToast("Landing initiated");
                });
            }
        });

        btnLeft = (Button) findViewById(R.id.btnLeft);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mFlightController.
                if (fMode.equals(FlightMode.JOYSTICK)) {
                    //aBoolean == true means virtual stick mode is enabled
                    mFlightController.setRollPitchControlMode(RollPitchControlMode.VELOCITY);
                    mFlightController.setYawControlMode(YawControlMode.ANGULAR_VELOCITY);
                    mFlightController.setVerticalControlMode(VerticalControlMode.VELOCITY);
                    mFlightController.setRollPitchCoordinateSystem(FlightCoordinateSystem.BODY);
                    if (null == mSendVirtualStickDataTimer) {
                        mSendVirtualStickDataTask = new SendVirtualStickDataTask(20, 2.05f, 0.00f, 0.00f, 0.00f);
                        mSendVirtualStickDataTimer = new Timer();
                        mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);
                    }

                    return;
                } else {
                    // virtual stick mode is disabled proceed to enable
                    mFlightController.setVirtualStickModeEnabled(true, djiError -> {
                        if (djiError != null) {
                            String message = "[WARN] Velocity Set service finished with state FAILURE. DJI rejected Joystick mode with error: " + djiError.toString();
                            showToast(message);
                            Log.d("djiErr", message);
                            return;
                        } else {
                            if (null == mSendVirtualStickDataTimer) {
                                mSendVirtualStickDataTask = new SendVirtualStickDataTask(20, 2.05f, 0.00f, 0.00f, 0.00f);
                                mSendVirtualStickDataTimer = new Timer();
                                mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);
                            }
                            String message = "[INFO] Velocity Set Service finished with state SUCCEEDED";
                            showToast(message);
                            Log.d("djiSuccess", message);
                            return;
                        }
                    });
                }
            }
        });

        btnRight = (Button) findViewById(R.id.btnRight);
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mFlightController.
                if (fMode.equals(FlightMode.JOYSTICK)) {
                    //aBoolean == true means virtual stick mode is enabled
                    mFlightController.setRollPitchControlMode(RollPitchControlMode.VELOCITY);
                    mFlightController.setYawControlMode(YawControlMode.ANGULAR_VELOCITY);
                    mFlightController.setVerticalControlMode(VerticalControlMode.VELOCITY);
                    mFlightController.setRollPitchCoordinateSystem(FlightCoordinateSystem.BODY);
                    if (null == mSendVirtualStickDataTimer) {
                        mSendVirtualStickDataTask = new SendVirtualStickDataTask(20, -2.05f, 0.00f, 0.00f, 0.00f);
                        mSendVirtualStickDataTimer = new Timer();
                        mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);
                    }

                    return;
                } else {
                    // virtual stick mode is disabled proceed to enable
                    mFlightController.setVirtualStickModeEnabled(true, djiError -> {
                        if (djiError != null) {
                            String message = "[WARN] Velocity Set service finished with state FAILURE. DJI rejected Joystick mode with error: " + djiError.toString();
                            showToast(message);
                            Log.d("djiErr", message);
                            return;
                        } else {
                            if (null == mSendVirtualStickDataTimer) {
                                mSendVirtualStickDataTask = new SendVirtualStickDataTask(20, -2.05f, 0.00f, 0.00f, 0.00f);
                                mSendVirtualStickDataTimer = new Timer();
                                mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);
                            }
                            String message = "[INFO] Velocity Set Service finished with state SUCCEEDED";
                            showToast(message);
                            Log.d("djiSuccess", message);
                            return;
                        }
                    });
                }
            }
        });

        btnUp = (Button) findViewById(R.id.btnUp);
        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mFlightController.
                if (fMode.equals(FlightMode.JOYSTICK)) {
                    //aBoolean == true means virtual stick mode is enabled
                    mFlightController.setRollPitchControlMode(RollPitchControlMode.VELOCITY);
                    mFlightController.setYawControlMode(YawControlMode.ANGULAR_VELOCITY);
                    mFlightController.setVerticalControlMode(VerticalControlMode.VELOCITY);
                    mFlightController.setRollPitchCoordinateSystem(FlightCoordinateSystem.BODY);
                    if (null == mSendVirtualStickDataTimer) {
                        mSendVirtualStickDataTask = new SendVirtualStickDataTask(20, 0.00f, 0.00f, 0.00f, 2.00f);
                        mSendVirtualStickDataTimer = new Timer();
                        mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);
                    }

                    return;
                } else {
                    // virtual stick mode is disabled proceed to enable
                    mFlightController.setVirtualStickModeEnabled(true, djiError -> {
                        if (djiError != null) {
                            String message = "[WARN] Velocity Set service finished with state FAILURE. DJI rejected Joystick mode with error: " + djiError.toString();
                            showToast(message);
                            Log.d("djiErr", message);
                            return;
                        } else {
                            if (null == mSendVirtualStickDataTimer) {
                                mSendVirtualStickDataTask = new SendVirtualStickDataTask(20, -2.05f, 0.00f, 0.00f, 2.00f);
                                mSendVirtualStickDataTimer = new Timer();
                                mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);
                            }
                            String message = "[INFO] Velocity Set Service finished with state SUCCEEDED";
                            showToast(message);
                            Log.d("djiSuccess", message);
                            return;
                        }
                    });
                }
            }
        });

        btnDown = (Button) findViewById(R.id.btnDown);
        btnDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mFlightController.
                if (fMode.equals(FlightMode.JOYSTICK)) {
                    //aBoolean == true means virtual stick mode is enabled
                    mFlightController.setRollPitchControlMode(RollPitchControlMode.VELOCITY);
                    mFlightController.setYawControlMode(YawControlMode.ANGULAR_VELOCITY);
                    mFlightController.setVerticalControlMode(VerticalControlMode.VELOCITY);
                    mFlightController.setRollPitchCoordinateSystem(FlightCoordinateSystem.BODY);
                    if (null == mSendVirtualStickDataTimer) {
                        mSendVirtualStickDataTask = new SendVirtualStickDataTask(20, 0.00f, 0.00f, 0.00f, -2.00f);
                        mSendVirtualStickDataTimer = new Timer();
                        mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);
                    }

                    return;
                } else {
                    // virtual stick mode is disabled proceed to enable
                    mFlightController.setVirtualStickModeEnabled(true, djiError -> {
                        if (djiError != null) {
                            String message = "[WARN] Velocity Set service finished with state FAILURE. DJI rejected Joystick mode with error: " + djiError.toString();
                            showToast(message);
                            Log.d("djiErr", message);
                            return;
                        } else {
                            if (null == mSendVirtualStickDataTimer) {
                                mSendVirtualStickDataTask = new SendVirtualStickDataTask(20, -2.05f, 0.00f, 0.00f, -2.00f);
                                mSendVirtualStickDataTimer = new Timer();
                                mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);
                            }
                            String message = "[INFO] Velocity Set Service finished with state SUCCEEDED";
                            showToast(message);
                            Log.d("djiSuccess", message);
                            return;
                        }
                    });
                }
            }
        });


        btnPos = (Button) findViewById(R.id.btnPos);
        btnPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDroneLocation();
                cameraUpdate();
            }
        });

    }

    private void fetchDroneHwId() {
        mFlightController.getSerialNumber(new CommonCallbacks.CompletionCallbackWith<String>() {
            @Override
            public void onSuccess(String s) {
                hardwareId = s;
                Log.d("hwid", "hwid Hardware ID: " + hardwareId);

                showToast("hwid Hardware ID: " + hardwareId);

                //deviceCheck();
            }

            @Override
            public void onFailure(DJIError djiError) {
                Log.d("hwid", "Hardware ID could not be fetched: " + djiError.toString());
                showToast("Hardware ID could not be fetched: " + djiError.toString());
            }
        });
    }

    private void cameraUpdate() {
        LatLng pos = new LatLng(droneLocationLat, droneLocationLng);
        float zoomlevel = (float) 18.0;
        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(pos, zoomlevel);
        gMap.moveCamera(cu);
    }

    //move onProductConnectionChange
    private void initDji() {
        mFlightController = getAircraftInstance().getFlightController();
        mFlightController.setRollPitchControlMode(RollPitchControlMode.VELOCITY);
        mFlightController.setYawControlMode(YawControlMode.ANGULAR_VELOCITY);
        mFlightController.setVerticalControlMode(VerticalControlMode.VELOCITY);
        mFlightController.setMultipleFlightModeEnabled(true, djiError -> {
            if (djiError == null) {
//                    setResultToToast("Multiple Flight Modes have been enabled");
                showToast("Multiple Flight Modes have been enabled");
            } else {
//                    setResultToToast("Error enabling multiple flight modes: " + djiError.getDescription());
            }
        });

        mFlightController.setMaxFlightRadiusLimitationEnabled(false, djiError -> {

        });

        mFlightController.setStateCallback(DJIFlightstateCallback);

        List<Battery> batteries_temp = getAircraftInstance().getBatteries();
        if (null != batteries_temp && !batteries_temp.isEmpty()) {
            List<Battery> batteries = Arrays.asList(new Battery[batteries_temp.size()]);
            for (Battery b : batteries_temp) {
                if (b.getIndex() >= batteries.size()) {
                    batteries.set(0, b);
                } else {
                    batteries.set(b.getIndex(), b);
                }
            }
            // logger.debug("Number of batteries found: " + batteries.size());
            Log.d("Battery", "No. of Batteries " + batteries.size());
            batteries.get(0).setStateCallback(DJIBatteryStateCallback);
        } /*else {
            logger.error("battery instance not found");
            handler.postDelayed(() -> {
                List<Battery> batteries_temp_1 = getAircraftInstance().getBatteries();
                if (null != batteries_temp_1 && !batteries_temp_1.isEmpty()) {
                    List<Battery> batteries = Arrays.asList(new Battery[batteries_temp_1.size()]);
                    for (Battery b : batteries_temp_1) {
                        if (b.getIndex() >= batteries.size()) {
                            batteries.set(0, b);
                        } else {
                            batteries.set(b.getIndex(), b);
                        }
                    }
                    logger.debug("Number of batteries found: " + batteries.size());
                    batteries.get(0).setStateCallback(DJIBatteryStateCallback);
                } else {
                    logger.error("battery instance not found again");
                }
            }, 2000);*/
        //}
    }

    private void startSDKRegistration() {
        Log.d("startSDKRegistration", "inside");
        DJISDKManager.getInstance().registerApp(MainActivity.this.getApplicationContext(), registrationCallback);
    }

    public synchronized BaseProduct getProductInstance() {

        return DJISDKManager.getInstance().getProduct();
    }

    public synchronized Aircraft getAircraftInstance() {
        BaseProduct product = getProductInstance();
        String modelName = product.getModel().getDisplayName();
        showToast("Aircraft Model Connected:" + modelName);
        return (Aircraft) product;
    }


    private void showToast(final String toastMsg) {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private static void stopVirtualStickDataCallback() {
        if (null != mSendVirtualStickDataTimer) {
            mSendVirtualStickDataTask.cancel();
            mSendVirtualStickDataTask = null;
            mSendVirtualStickDataTimer.cancel();
            mSendVirtualStickDataTimer.purge();
            mSendVirtualStickDataTimer = null;
        }
    }


    private void sendServiceResponseToServer(String id, String message, Boolean success, Service srv) {
        Log.d("MainActivity", "sendServiceResponseToServer: ");
        if (null == srv) {
            Log.d("MainActivity", "cannot send service response as handler is null");
            showToast("cannot send service response as handler is null");
            return;
        }
        try {
            JSONObject values = new JSONObject();
            values.put("message", message);
            values.put("success", success);
            ServiceResponse res = new ServiceResponse(values.toString(), true);
            srv.sendResponse(res, id);
        } catch (Exception e) {
            Log.d("MainActivity", "sendServiceResponseToServer Exception raised: " + e.toString());
        }
    }

    @Override
    public void sendFailureResponseToServer(String id, String service, String message) {
        Log.d("CloudConnect", "sendFailureResponseToServer: ");
        try {
            JSONObject data = new JSONObject();
            data.put("op", "service_response");
            data.put("id", id);
            data.put("service", service);
            data.put("result", true);
            JSONObject values = new JSONObject();
            values.put("message", "[WARN] " + message);
            values.put("success", false);
            data.put("values", values);
            socket.send(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendSuccessResponseToServer(String id, String service, String message) {
        Log.d("CloudConnect", "sendSuccessResponseToServer: ");
        try {
            JSONObject data = new JSONObject();
            data.put("op", "service_response");
            data.put("id", id);
            data.put("service", service);
            data.put("result", true);
            JSONObject values = new JSONObject();
            values.put("message", message);
            values.put("success", true);
            data.put("values", values);
            socket.send(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void replyNamespace(String id, String service) {
        //showToast("get namespace");
        Log.d("replyNamespace", "replyNamespace: ");
        try {
            JSONObject data = new JSONObject();
            data.put("op", "service_response");
            data.put("id", id);
            data.put("service", service);
            data.put("result", true);

            JSONObject values = new JSONObject();
            values.put("message", "FlytOS namespace is flytos");
            values.put("success", true);

            JSONObject paramInfo = new JSONObject();
            paramInfo.put("param_value", "flytos");
            paramInfo.put("param_id", "global_namespace");
            values.put("param_info", paramInfo);

            data.put("values", values);
            socket.send(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void land(String id, String service, boolean async) {
        showToast("LAND");
        /*if (null != aircraftInst && aircraftInst.isConnected()) {

            if (!isFlying) {
                try {
                    JSONObject data = new JSONObject();
                    data.put("op", "service_response");
                    data.put("id", id);
                    data.put("service", service);
                    data.put("result", true);
                    JSONObject values = new JSONObject();
                    values.put("message", "[WARN] Land service REJECTED. Vehicle NOT in In-Air state.");
                    values.put("success", false);
                    data.put("values", values);
                    socket.send(data);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/

        mFlightController.startLanding(djiError -> {
            try {
                JSONObject data = new JSONObject();
                data.put("op", "service_response");
                data.put("id", id);
                data.put("service", service);
                data.put("result", true);
                if (djiError != null) {
                    JSONObject values = new JSONObject();
                    values.put("message", "[WARN] Land service finished with state FAILURE: " + djiError.toString());
                    values.put("success", false);
                    data.put("values", values);
                } else {
                    JSONObject values = new JSONObject();
                    values.put("message", "[INFO] Land service finished with state SUCCEEDED");
                    values.put("success", true);
                    data.put("values", values);
                }
                socket.send(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void takeOff(String id, String service, double takeOffAlt) {
        stopVirtualStickDataCallback();
        showToast("Take Off");

            /*if (isFlying) {
                try {
                    JSONObject data = new JSONObject();
                    data.put("op", "service_response");
                    data.put("id", id);
                    data.put("service", service);
                    data.put("result", true);
                    JSONObject values = new JSONObject();
                    values.put("message", "[WARN] TakeOff service REJECTED. Vehicle already in In-Air state.");
                    values.put("success", false);
                    data.put("values", values);
                    socket.send(data);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/

        mFlightController.startTakeoff(
                djiError -> {
                    try {
                        JSONObject data = new JSONObject();
                        data.put("op", "service_response");
                        data.put("id", id);
                        data.put("service", service);
                        data.put("result", true);
                        if (djiError != null) {
                            JSONObject values = new JSONObject();
                            values.put("message", "[WARN] Takeoff service finished with state FAILURE");
                            values.put("success", false);
                            data.put("values", values);
                        } else {
                            JSONObject values = new JSONObject();
                            values.put("message", "[INFO] Takeoff service finished with state SUCCEEDED");
                            values.put("success", true);
                            data.put("values", values);
                        }
                        socket.send(data);
                    } catch (Exception e) {

                    }
                }
        );
    }

    @Override
    public void velocitySet(String id, String service, float x, float y, float z, float yaw_rate, boolean yaw_rate_valid, boolean isRelative, boolean isBodyFrame) {
        //if (null != aircraftInst && aircraftInst.isConnected()) {
            /*if (!isFlying) {
                sendFailureResponseToServer(id, service, "Velocity Set service REJECTED. Vehicle NOT In-Air state.");
                return;
            }*/

        if (yaw_rate_valid) {
            mYaw = (float) Math.toDegrees(yaw_rate);
            // DJI accepts yawrate max of 100 degrees/s
            mYaw = (float) min(99.0, max(-99.0, mYaw));
        } else {
            mYaw = 0;
        }
        mPitch = y;
        mRoll = x;
        mThrottle = -z;
        if (!isRelative && isBodyFrame) {
            mCoordinateSystem = FlightCoordinateSystem.BODY;
        } else {
            mCoordinateSystem = FlightCoordinateSystem.GROUND;
        }
        if (isRelative) {
            mPitch = y + mFlightController.getState().getVelocityY();
            mRoll = x + mFlightController.getState().getVelocityX();
            mThrottle = z + mFlightController.getState().getVelocityZ();
        }

        mFlightController.setRollPitchControlMode(RollPitchControlMode.VELOCITY);
        mFlightController.setYawControlMode(YawControlMode.ANGULAR_VELOCITY);
        mFlightController.setVerticalControlMode(VerticalControlMode.VELOCITY);
        mFlightController.setRollPitchCoordinateSystem(mCoordinateSystem);

        if (fMode.equals(FlightMode.JOYSTICK)) {
            //aBoolean == true means virtual stick mode is enabled
            if (null == mSendVirtualStickDataTimer) {
                mSendVirtualStickDataTask = new SendVirtualStickDataTask(20, mPitch, mRoll, mYaw, mThrottle);
                mSendVirtualStickDataTimer = new Timer();
                mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);
            }
            //handler.removeCallbacksAndMessages(null);

            String message = "[INFO] Velocity Set Service finished with state SUCCEEDED";
            sendSuccessResponseToServer(id, service, message);
            return;
        } else {
            // virtual stick mode is disabled proceed to enable
            mFlightController.setVirtualStickModeEnabled(true, djiError -> {
                showToast("enabling virtual stick mode");
                if (djiError != null) {
                    String message = "[WARN] Velocity Set service finished with state FAILURE. DJI rejected Joystick mode with error: " + djiError.toString();
                    stopVirtualStickDataCallback();
                    sendFailureResponseToServer(id, service, message);
                    return;
                } else {
                    if (null == mSendVirtualStickDataTimer) {
                        mSendVirtualStickDataTask = new SendVirtualStickDataTask(20, mPitch, mRoll, mYaw, mThrottle);
                        mSendVirtualStickDataTimer = new Timer();
                        mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 0, 100);
                    }
                    //handler.removeCallbacksAndMessages(null);
                    String message = "[INFO] Velocity Set Service finished with state SUCCEEDED";
                    sendSuccessResponseToServer(id, service, message);
                    return;
                }
            });
        }

//        mFlightController.setVirtualStickModeEnabled(false, djiError -> mFlightController.setVirtualStickModeEnabled(true, djiError1 -> {
//            try {
//                JSONObject data = new JSONObject();
//                data.put("op", "service_response");
//                data.put("id", id);
//                data.put("service", service);
//                data.put("result", true);
//                if (djiError1 != null) {
//                    JSONObject values = new JSONObject();
//                    values.put("message", "[WARN] Velocity Set service finished with state FAILURE. DJI rejected Joystick mode.");
//                    values.put("success", false);
//                    data.put("values", values);
//                } else {
//                    JSONObject values = new JSONObject();
//                    values.put("message", "[INFO] Velocity Set service finished with state SUCCEEDED");
//                    values.put("success", true);
//                    data.put("values", values);
//                    if (null == mSendVirtualStickDataTimer) {
//                        mSendVirtualStickDataTask = new SendVirtualStickDataTask();
//                        mSendVirtualStickDataTimer = new Timer();
//                        mSendVirtualStickDataTimer.schedule(mSendVirtualStickDataTask, 100, 100);
//                    }
//                    if (handler != null) {
//                        handler.removeCallbacksAndMessages(null);
//                    }
//                }
//                socket.send(data);
//            } catch (Exception e) {
//
//            }
//        }));

        /*} else {
            sendFailureResponseToServer(id, service, "FLIGHT_NOT_CONNECTED");
        }*/
    }

    @Override
    public void waypointSet(String id, String service, JSONObject args) {
        stopVirtualStickDataCallback();
        showToast("Waypoint SET");

//            if (null != aircraftInst && aircraftInst.isConnected()) {
//            if (waypointMissionBuilder == null) {
        waypointMissionBuilder = new WaypointMission.Builder();
        getWaypointMissionOperator().getLoadedMission();
        getWaypointMissionOperator().downloadMission(djiError -> {

        });
//            }
        /*Clear Waypoints*/
        waypointList.clear();
        Log.d("waypointset", "wp current state 1: " + getWaypointMissionOperator().getCurrentState());
        //Check if already executing mission
        if (getWaypointMissionOperator().getCurrentState() == WaypointMissionState.EXECUTING) {
            getWaypointMissionOperator().stopMission(error -> {
                if (error == null) {
                    handler.postDelayed(() -> {
                        try {
                            loadMission(id, service, args);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, 200);
                } else {
                    sendFailureResponseToServer(id, service, "Waypoint Set service stop mission finished with state FAILURE due to an error: " + error.getDescription());
                }
            });
        } else {
            //load a fresh waypoint
            try {
                loadMission(id, service, args);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
           /* } else {
                sendFailureResponseToServer(id, service, MSG_FLIGHT_NOT_CONNECTED);
                if (hasDroneConnection.get()) notifyDroneStatusChange();
            }*/

    }

    @Override
    public void waypointExecute(String id, String service) {
        stopVirtualStickDataCallback();
        showToast("Waypoint Execute");
        //if (null != aircraftInst && aircraftInst.isConnected()) {
        if (getWaypointMissionOperator().getCurrentState() == WaypointMissionState.EXECUTION_PAUSED) {
            //Resume Execution
            getWaypointMissionOperator().resumeMission(error -> {
                if (error == null) {
                    sendSuccessResponseToServer(id, service, "[mayurINFO] Waypoint(Resume) Execute service finished with state SUCCEEDED");
                } else {
                    sendFailureResponseToServer(id, service, "Waypoint(Resume) Execute service resume finished with state FAILURE due to an error: " + error.getDescription());
                }
            });
        } else if (getWaypointMissionOperator().getCurrentState() == WaypointMissionState.READY_TO_EXECUTE) {
            //Start Execution
            getWaypointMissionOperator().startMission(error -> {
                if (error == null) {
                    sendSuccessResponseToServer(id, service, "[mayurINFO] Waypoint(Start) Execute service finished with state SUCCEEDED");
                } else {
                    sendFailureResponseToServer(id, service, "Waypoint(Start) Execute service start finished with state FAILURE due to an error: " + error.getDescription());
                }
            });
        } else {
            sendFailureResponseToServer(id, service, "[ERROR] Waypoint Execute service finished with state FAILURE due to an error: Waypoint Mission in Incorrect State: " + getWaypointMissionOperator().getCurrentState());
        }
            /*} else {
                sendFailureResponseToServer(id, service, MSG_FLIGHT_NOT_CONNECTED);
                if (hasDroneConnection.get()) notifyDroneStatusChange();
            }*/

    }

    @Override
    public void waypointPause(final String id, final String service) {

        stopVirtualStickDataCallback();
        showToast("Waypoint Pause");
//        setResultToToast(getWaypointMissionOperator().getCurrentState().toString());
        //if (null != aircraftInst && aircraftInst.isConnected()) {
        if (getWaypointMissionOperator().getCurrentState() != WaypointMissionState.EXECUTING) {
            getWaypointMissionOperator().pauseMission(error -> sendSuccessResponseToServer(id, service, "[INFO] Waypoint Pause service finished with state SUCCEEDED"));
        } else {
            getWaypointMissionOperator().pauseMission(error -> {
                if (error == null) {
                    sendSuccessResponseToServer(id, service, "[INFO] Waypoint Pause service finished with state SUCCEEDED");
                } else {
                    sendFailureResponseToServer(id, service, "Waypoint Pause service finished with state FAILURE due to an error: " + error.getDescription());
                }
            });
        }
        /*} else {
            sendFailureResponseToServer(id, service, MSG_FLIGHT_NOT_CONNECTED);
            if (hasDroneConnection.get()) notifyDroneStatusChange();
        }*/
    }

    @Override
    public void waypointGet(final String id, final String service) {

        stopVirtualStickDataCallback();
        showToast("Waypoint GET");
        //if (null != aircraftInst && aircraftInst.isConnected()) {
        try {
            JSONObject data = new JSONObject();
            data.put("op", "service_response");
            data.put("id", id);
            data.put("service", service);
            data.put("result", true);
            JSONObject values = new JSONObject();
            JSONArray waypointArr = new JSONArray();
            for (int i = 0; i < waypointList.size(); i++) {
                JSONObject waypointObj = new JSONObject();
                waypointObj.put("frame", 3);
                waypointObj.put("command", 16);
                waypointObj.put("is_current", false);
                waypointObj.put("autocontinue", true);
                waypointObj.put("param1", 0);
                waypointObj.put("param2", 0);
                waypointObj.put("param3", 0);
                waypointObj.put("param4", 0);
                waypointObj.put("x_lat", waypointList.get(i).coordinate.getLatitude());
                waypointObj.put("y_long", waypointList.get(i).coordinate.getLongitude());
                waypointObj.put("z_alt", waypointList.get(i).altitude);
                waypointArr.put(waypointObj);
            }
            values.put("waypoints", waypointArr);
            values.put("success", true);
            values.put("message", "[INFO] Waypoint Get service finished with state SUCCEEDED");
            values.put("wp_received", waypointList.size());
            data.put("values", values);
            socket.send(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
       /* } else {
            sendFailureResponseToServer(id, service, MSG_FLIGHT_NOT_CONNECTED);
            if (hasDroneConnection.get()) notifyDroneStatusChange();
        }*/
    }

    @Override
    public void waypointClear(String id, String service) {
        stopVirtualStickDataCallback();
        showToast("Waypoint Clear");
        //if (null != aircraftInst && aircraftInst.isConnected()) {
            try {
                waypointList.clear();
                Log.d("waypointClear: ","wp current state: " + getWaypointMissionOperator().getCurrentState());
                getWaypointMissionOperator().stopMission(error -> {
                    if (error == null && null != id) {
                        sendSuccessResponseToServer(id, service, "[INFO] Waypoint Clear service finished with state SUCCEEDED");
                    } else {
                        if (null != id) {
                            sendFailureResponseToServer(id, service, "Waypoint Clear service finished with state FAILURE due to an error: " + getWaypointMissionOperator().getCurrentState() + error.getDescription());
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        /*} else {
            sendFailureResponseToServer(id, service, MSG_FLIGHT_NOT_CONNECTED);
            if (hasDroneConnection.get()) notifyDroneStatusChange();
        }*/

    }

    private void loadMission(String id, String service, JSONObject args) {
        try {
            Map<Integer, String> wpCameraModeMapTemp = new HashMap<>();

            /*Set Config values*/
            WaypointMissionFinishedAction mFinishedAction = WaypointMissionFinishedAction.NO_ACTION;
            WaypointMissionHeadingMode mHeadingMode = WaypointMissionHeadingMode.AUTO;
            /*Set waypoints*/
            JSONArray wayPointsArr = args.getJSONArray("waypoints");
            float mSpeed = 5.0f;
            WaypointMissionFlightPathMode fpMode = WaypointMissionFlightPathMode.NORMAL;

            for (int i = 0; i < wayPointsArr.length(); i++) {

                JSONObject wayPointData = wayPointsArr.getJSONObject(i);
                int command = wayPointData.getInt("command");

                if (command == 16 || command == 20 || command == 21) {
                    float alt = (float) wayPointData.getDouble("z_alt");
                    Waypoint mWaypoint = new Waypoint(wayPointData.getDouble("x_lat"), wayPointData.getDouble("y_long"), alt);
                    float cornerRadius = (float) wayPointData.getDouble("param3");
                    if (cornerRadius > 0) {
                        fpMode = WaypointMissionFlightPathMode.CURVED;
                        mWaypoint.cornerRadiusInMeters = cornerRadius;
                    }
                    int stay_s = wayPointData.getInt("param1");
                    if (stay_s > 0)
                        mWaypoint.addAction(new WaypointAction(WaypointActionType.STAY, stay_s));
                    waypointList.add(mWaypoint);
                }

                // change FlightSpeed
                if (command == 178) {
                    mSpeed = (float) wayPointData.getDouble("param2");
                }

                // camera trigger distance
                /*if (command == 206) {
                    if (waypointList.size() == 0) {
                        sendFailureResponseToServer(id, service, "Waypoint Set service finished with state FAILURE due to an error: No valid waypoint received before Camera Trigger Distance command command.");
                        return;
                    }

                    if (null == cameraInst) {
                        sendFailureResponseToServer(id, service, "Waypoint Set service finished with state FAILURE due to an error: Camera action command received, but camera NOT detected.");
                        return;
                    } else {
                        if (camIsVideoRecording && camMode == SettingsDefinitions.CameraMode.RECORD_VIDEO) {
                            cameraInst.stopRecordVideo(djiError -> cameraInst.setMode(SettingsDefinitions.CameraMode.SHOOT_PHOTO, error -> {
                                if (error == null) {
                                    Log.d("Dashboard Activity", "Set cameraMode to Shoot Photo success");
//                                    setResultToToast("Set cameraMode to Shoot Photo success");
                                } else {
                                    Log.e("Dashboard Activity", "Set cameraMode to Shoot Photo failed");
//                                    setResultToToast("Set cameraMode to Shoot Photo failed");
                                }
                            }));
                        } else if (camMode != SettingsDefinitions.CameraMode.SHOOT_PHOTO) {
                            cameraInst.setMode(SettingsDefinitions.CameraMode.SHOOT_PHOTO, error -> {
                                if (error == null) {
                                    Log.d("Dashboard Activity", "Set cameraMode to Shoot Photo success");
//                                    setResultToToast("Set cameraMode to Shoot Photo success");
                                } else {
                                    Log.e("Dashboard Activity", "Set cameraMode to Shoot Photo failed");
//                                    setResultToToast("Set cameraMode to Shoot Photo failed");
                                }
                            });
                        }
                    }

                    float shootDist = (float) wayPointData.getDouble("param1");
                    if (shootDist > 0.1) {
                        waypointList.get(waypointList.size() - 1).shootPhotoDistanceInterval = shootDist;
                    }
                }*/

                // gimbal rotate
                if (command == 205) {
                    if (waypointList.size() == 0) {
                        sendFailureResponseToServer(id, service, "Waypoint Set service finished with state FAILURE due to an error: No valid waypoint received before GimbalPitch command.");
                        return;
                    }
                    int gimbal_pitch = wayPointData.getInt("param1");
                    if (gimbal_pitch < -90 || gimbal_pitch > 0) {
                        //throw ERROR. and return.
                        sendFailureResponseToServer(id, service, "Waypoint Set service finished with state FAILURE due to an error: Gimbal Pitch must be between [-90,0].");
                        return;
                    }
                    waypointList.get(waypointList.size() - 1).addAction(new WaypointAction(WaypointActionType.GIMBAL_PITCH, gimbal_pitch));
                }

                // yaw action
                if (command == 115) {
                    if (waypointList.size() == 0) {
                        sendFailureResponseToServer(id, service, "Waypoint Set service finished with state FAILURE due to an error: No valid waypoint received before GimbalPitch command.");
                        return;
                    }
                    int yaw = wayPointData.getInt("param1");
                    yaw = yaw % 360;
                    if (yaw > 180) {
                        yaw -= 360;
                    }
                    waypointList.get(waypointList.size() - 1).addAction(new WaypointAction(WaypointActionType.ROTATE_AIRCRAFT, yaw));
                }

                // camera Shoot Photo
                if (command == 2000) {
                    if (waypointList.size() == 0) {
                        sendFailureResponseToServer(id, service, "Waypoint Set service finished with state FAILURE due to an error: No valid waypoint received before Camera trigger command.");
                        return;
                    }
                    waypointList.get(waypointList.size() - 1).addAction(new WaypointAction(WaypointActionType.START_TAKE_PHOTO, 1));
                }

                // video record start
                if (command == 2500) {
                    if (waypointList.size() == 0) {
                        sendFailureResponseToServer(id, service, "Waypoint Set service finished with state FAILURE due to an error: No valid waypoint received before Start Video record command.");
                        return;
                    }
                    waypointList.get(waypointList.size() - 1).addAction(new WaypointAction(WaypointActionType.START_RECORD, 1));
                }

                // video record stop
                if (command == 2501) {
                    if (waypointList.size() == 0) {
                        sendFailureResponseToServer(id, service, "Waypoint Set service finished with state FAILURE due to an error: No valid waypoint received before Stop Video record command.");
                        return;
                    }
                    waypointList.get(waypointList.size() - 1).addAction(new WaypointAction(WaypointActionType.STOP_RECORD, 1));
                }

                // hover in place
                if (command == 19) {
                    if (waypointList.size() == 0) {
                        sendFailureResponseToServer(id, service, "Waypoint Set service finished with state FAILURE due to an error: No valid waypoint received before Hover/Stay command.");
                        return;
                    }
                    waypointList.get(waypointList.size() - 1).addAction(new WaypointAction(WaypointActionType.STAY, wayPointData.getInt("param1")));
                }

                // special camera mode to change IR settings too
                if (command == 530) {
                    if (waypointList.size() == 0) {
                        sendFailureResponseToServer(id, service, "Waypoint Set service finished with state FAILURE due to an error: No valid waypoint received before Stop Video record command.");
                        return;
                    }
                    wpCameraModeMapTemp.put(waypointList.size() - 1, "" + wayPointData.getInt("param2") + ";" + wayPointData.getInt("param3") + ";" + wayPointData.getInt("param4"));
                }

                // finished action
                if (i == (wayPointsArr.length() - 1)) {
                    if (command == 20) {
                        mFinishedAction = WaypointMissionFinishedAction.GO_HOME;
                    } else if (command == 21) {
                        mFinishedAction = WaypointMissionFinishedAction.AUTO_LAND;
                    }
                }
            }

            if (mSpeed <= 0) {
                mSpeed = (float) 5.0;
            }

            if (mSpeed > MAX_AUTO_FLIGHT_SPEED) {
                mSpeed = MAX_AUTO_FLIGHT_SPEED;
            }

            double maxSpeed = 2.0 * mSpeed;
            if (maxSpeed > MAX_FLIGHT_SPEED) {
                maxSpeed = MAX_FLIGHT_SPEED;
            }
            waypointMissionBuilder.finishedAction(mFinishedAction)
                    .headingMode(mHeadingMode)
                    .autoFlightSpeed(mSpeed)
                    .maxFlightSpeed((float) maxSpeed)
                    .gotoFirstWaypointMode(WaypointMissionGotoWaypointMode.SAFELY)
                    .flightPathMode(fpMode)
                    .waypointList(waypointList)
                    .waypointCount(waypointList.size());

            WaypointMission mission = waypointMissionBuilder.build();
//            DJIError missionError = mission.checkParameters();
//            if (missionError == null) {
            DJIError error = getWaypointMissionOperator().loadMission(mission);
            Log.d("loadMission", "wp current state: " + getWaypointMissionOperator().getCurrentState());
            if (error == null) {
                uploadMission(id, service);
                // wpCameraModeMap = wpCameraModeMapTemp;
            } else {
                sendFailureResponseToServer(id, service, "Waypoint Set service finished during load with state FAILURE due to an error: " + error.getDescription() + " " + getWaypointMissionOperator().getCurrentState());
            }
//            } else {
//                sendFailureResponseToServer(id, service, "Please check your parameters mission load Error: " + missionError.getDescription());
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadMission(final String id, final String service) {
        Log.d("uploadMission", "calling upload mission: " + id);
        if (getWaypointMissionOperator().getCurrentState() == WaypointMissionState.READY_TO_UPLOAD || getWaypointMissionOperator().getCurrentState() == WaypointMissionState.READY_TO_RETRY_UPLOAD) {
            getWaypointMissionOperator().uploadMission(error -> {
                if (error == null) {
                    sendSuccessResponseToServer(id, service, "[INFO] Waypoint Set service finished with state SUCCEEDED");
                } else {
                    sendFailureResponseToServer(id, service, "Waypoint Set service finished with state FAILURE due to an error: " + error.getDescription());
                }
            });
        } else {
            handler.postDelayed(() -> uploadMission(id, service), 200);
        }
    }

    public static class SendVirtualStickDataTask extends TimerTask {
        int counter;
        float mPitch;
        float mRoll;
        float mYaw;
        float mThrottle;

        SendVirtualStickDataTask(int c, float mPitch, float mRoll, float mYaw, float mThrottle) {
            counter = c;
            this.mPitch = mPitch;
            this.mRoll = mRoll;
            this.mYaw = mYaw;
            this.mThrottle = mThrottle;
        }

        @Override
        public void run() {
            {
                if (null != mFlightController) {
                    if (counter == 0) {
                        stopVirtualStickDataCallback();
                        return;
                    } else if (counter > 0) {
                        counter--;
                    }
                    mFlightController.sendVirtualStickFlightControlData(
                            new FlightControlData(
                                    mPitch, mRoll, mYaw, mThrottle
                            ), djiError -> {
                                if (djiError != null)
                                    Log.d("djiErr", djiError.getDescription());
                            }
                    );
                }
            }
        }
    }

    public WaypointMissionOperator getWaypointMissionOperator() {
        if (instance == null) {
            if (DJISDKManager.getInstance().getMissionControl() != null) {
                instance = DJISDKManager.getInstance().getMissionControl().getWaypointMissionOperator();
            }
        }
        return instance;
    }

    private void configWayPointMission() {

        if (waypointMissionBuilder == null) {

            waypointMissionBuilder = new WaypointMission.Builder().finishedAction(mFinishedAction)
                    .headingMode(mHeadingMode)
                    .autoFlightSpeed(mSpeed)
                    .maxFlightSpeed(mSpeed)
                    .flightPathMode(WaypointMissionFlightPathMode.NORMAL);

        } else {
            waypointMissionBuilder.finishedAction(mFinishedAction)
                    .headingMode(mHeadingMode)
                    .autoFlightSpeed(mSpeed)
                    .maxFlightSpeed(mSpeed)
                    .flightPathMode(WaypointMissionFlightPathMode.NORMAL);

        }

        if (waypointMissionBuilder.getWaypointList().size() > 0) {

            for (int i = 0; i < waypointMissionBuilder.getWaypointList().size(); i++) {
                waypointMissionBuilder.getWaypointList().get(i).altitude = altitude;
            }

            showToast("Set Waypoint attitude successfully");
        }

        DJIError error = getWaypointMissionOperator().loadMission(waypointMissionBuilder.build());
        if (error == null) {
            showToast("loadWaypoint succeeded");
        } else {
            showToast("loadWaypoint failed " + error.getDescription());
        }

    }

    //Add Listener for WaypointMissionOperator
    private void addListener() {
        if (getWaypointMissionOperator() != null) {
            getWaypointMissionOperator().addListener(eventNotificationListener);
        }
    }

    private void removeListener() {
        if (getWaypointMissionOperator() != null) {
            getWaypointMissionOperator().removeListener(eventNotificationListener);
        }
    }

    private WaypointMissionOperatorListener eventNotificationListener = new WaypointMissionOperatorListener() {
        @Override
        public void onDownloadUpdate(WaypointMissionDownloadEvent downloadEvent) {

        }

        @Override
        public void onUploadUpdate(WaypointMissionUploadEvent uploadEvent) {

        }

        @Override
        public void onExecutionUpdate(WaypointMissionExecutionEvent executionEvent) {

        }

        @Override
        public void onExecutionStart() {

        }

        @Override
        public void onExecutionFinish(@Nullable final DJIError error) {
            showToast("Execution finished: " + (error == null ? "Success!" : error.getDescription()));
        }
    };

    private void startWaypointMission() {

        getWaypointMissionOperator().startMission(error -> showToast(
                "Mission Start: " + (error == null ? "Successfully" : error.getDescription()))
        );

    }

    private void stopWaypointMission() {

        getWaypointMissionOperator().stopMission(error -> showToast(
                "Mission Stop: " + (error == null ? "Successfully" : error.getDescription()))
        );

    }

    @Override
    protected void onDestroy() {
        removeListener();
        publish5Hz_handler.removeCallbacks(publish5Hz_runnable);
        super.onDestroy();
    }


    private void login(final String email, final String password) {

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        JSONObject postBody = new JSONObject();
        try {
            postBody.put("email", email);
            postBody.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody body = RequestBody.create(JSON, postBody.toString());
        Request request = new Request.Builder()
                .url("https://my.flytbase.com/accounts/login/v2/")
                .post(body)
                .build();
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HeaderInterceptor())
                .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                Log.e("Login", "Login onFailure: " + e.toString());
                showToast("Login onFailure: " + e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    ResponseBody responseBody = response.body();
                    if (response.isSuccessful()) {
                        if (responseBody != null) {
                            JSONObject jsonObject = new JSONObject(responseBody.string());
                            token = jsonObject.getString("token");
                            Log.d("Token:", token);
                            showToast("Token: " + token);
                            /*SharedPreferences prefs = getSharedPreferences("FlytOS", MODE_PRIVATE);
                            prefs.edit().putString("token", token).apply();
                            prefs.edit().putString("email", AESHelper.encrypt("flyt-login", email)).apply();
                            prefs.edit().putString("password", AESHelper.encrypt("flyt-login", password)).apply();
                            startActivity(new Intent(LoginActivity.this, DashboardActivity.class));*/
                            //finish();
                            deviceCheck();
                        }
                    } else {
                        Log.d("Login error", response.toString());
                        if (responseBody != null) {
                            JSONObject jsonObject = new JSONObject(responseBody.string());
                            if (jsonObject.has("message")) {
                                showToast(jsonObject.getString("message"));
                                return;
                            }
                        }
                        showToast(response.code() + " " + response.message());
                    }
                } catch (Exception e) {
                    Log.e("Login error:", e.getMessage());
                }
            }
        });
    }

    private void deviceCheck() {
        Log.d("DeviceCheck", "Starting device check");

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient.Builder()
                //.addInterceptor(new TokenInterceptor(MainActivity.this))
                .build();
        JSONObject postBody = new JSONObject();
        try {
            postBody.put("hardware_id", hardwareId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(JSON, postBody.toString());
        final Request request = new Request.Builder()
                .header("Accept", "*/*")
                .header("User-Agent", "FLYTBASE_DJI_ANDROID")
                .header("Authorization", String.format("JWT %s", token))
                .url("https://my.flytbase.com/api/devices/get/by_hardware_id/v2/")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
                Log.d("DeviceCheck", "get hardware by ID: FAILED");
                Handler handler = new Handler(Looper.getMainLooper());
            }

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    boolean status = jsonObject.getBoolean("success");
                    if (status) {
                        Log.d("DeviceCheck", "get hardware by ID: SUCCESS");

                        serial_number = jsonObject.getString("license_key");
                        Log.d("DeviceCheck", "S.NO: " + serial_number);

                        guid = jsonObject.getString("device_id");
                        Log.d("DeviceCheck", "guid: " + guid);

                        String deviceName = jsonObject.getString("device_name");
                        Log.d("DeviceCheck", "device name: " + deviceName);

                        vehicleID = jsonObject.getString("vehicle_id");
                        Log.d("DeviceCheck", "vehicle ID: " + vehicleID);

                        initCloudConnection();
                        //rosCloudInitialize();
                    } else {
                        runOnUiThread(() -> {
                            Log.d("DeviceCheck", "get hardware by ID: NOT FOUND");
                            showToast("get hardware by ID: NOT FOUND");
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("DeviceCheck", "get hardware by ID: Exception raised: ");
                }
            }
        });
    }

    private synchronized void initCloudConnection() {

        WebSocketClient.Builder builder = WebSocketClient.Builder.with("wss://dev.flytbase.com:9000");
        builder.addHeader("dev_guid", guid);
        builder.addHeader("Content-Type", "application/json");
        builder.setPingInterval(5, TimeUnit.SECONDS);
        socket = builder.build();

        socket.onEvent(WebSocketClient.EVENT_OPEN, new WebSocketClient.OnEventListener() {
            @Override
            public void onMessage(String event) {
                Log.d("CloudConnect", "connection open " + event);
            }
        });

        socket.onEvent(WebSocketClient.EVENT_RECONNECT_ATTEMPT, new WebSocketClient.OnEventListener() {

            @Override
            public void onMessage(String event) {
                Log.d("CloudConnect", "connection reattempt " + event);
            }
        });

        socket.onEvent(WebSocketClient.EVENT_CLOSED, new WebSocketClient.OnEventListener() {
            @Override
            public void onMessage(String event) {
                Log.d("CloudConnect", "connection closed " + event);
            }
        });

        socket.addMessageListener(wsMsgListener);
        socket.setOnChangeStateListener(new WebSocketClient.OnStateChangeListener() {
            @Override
            public void onChange(WebSocketClient.State status) {
                Log.d("CloudConnect", "Websocket: onChange" + status.toString());
            }
        });
        socket.connect();

        publish2Hz_handler.removeCallbacks(publish2Hz_runnable);
        publish2Hz_handler.postDelayed(publish2Hz_runnable, 1000);
        publish5Hz_handler.removeCallbacks(publish5Hz_runnable);
        publish5Hz_handler.postDelayed(publish5Hz_runnable, 1000);
    }


    private void publish5Hz() {
        try {
            JSONObject header = new JSONObject();
            JSONObject stamp = new JSONObject();
            long epochTime = System.currentTimeMillis();
            stamp.put("secs", epochTime / 1000L);

            stamp.put("nsecs", (epochTime % 1000L) * 1e6);
            header.put("stamp", stamp);
            header.put("frame_id", "fcu");

            //send LPOS data
            JSONObject msg2 = new JSONObject();
            JSONObject twist2 = new JSONObject();
            JSONObject linear2 = new JSONObject();
            double x;
            double y;

            //if (!isNaN(homeLatitude) && !isNaN(homeLongitude) && !isNaN(latitude) && !isNaN(longitude) && !isNaN(altitude)) {
            double[] xy = MainApplication.calculateDistance(homeLatitude, homeLongitude, droneLocationLat, droneLocationLng);
            x = xy[0];
            y = xy[1];
            linear2.put("x", x); // I have to put x and y here
            linear2.put("y", y);
            linear2.put("z", altitude * -1);   //state.getAircraftLocation().getAltitude() replaced by altitude

            twist2.put("linear", linear2);
            JSONObject angular2 = new JSONObject();
            angular2.put("x", velocityX);  //here state.getVelocityX/Y/Z() has been
            angular2.put("y", velocityY);  //has been replaced by velocityX/Y/Z
            angular2.put("z", velocityZ);
            twist2.put("angular", angular2);
            msg2.put("twist", twist2);
            msg2.put("header", header);

            JSONObject data2 = new JSONObject();
            data2.put("topic", "/flytos/mavros/local_position/local");
            data2.put("msg", msg2);
            data2.put("op", "publish");
            socket.send(data2);


            //send GPOS data
            JSONObject msg3 = new JSONObject();

            msg3.put("altitude", altitude);
            msg3.put("longitude", droneLocationLng);
            msg3.put("latitude", droneLocationLat);

            JSONObject status = new JSONObject();
            status.put("status", sateliteCount);
            msg3.put("status", status);

            msg3.put("header", header);

            JSONObject data3 = new JSONObject();
            data3.put("topic", "/flytos/mavros/global_position/global");
            data3.put("msg", msg3);
            data3.put("op", "publish");
            socket.send(data3);

            //send IMU data
            JSONObject msg = new JSONObject();
            JSONObject twist = new JSONObject();
            JSONObject linear = new JSONObject();
            linear.put("x", Roll);
            linear.put("y", Pitch);
            linear.put("z", Yaw);

            twist.put("linear", linear);
            msg.put("twist", twist);
            msg.put("header", header);

            JSONObject data = new JSONObject();
            data.put("topic", "/flytos/mavros/imu/data_euler");
            data.put("msg", msg);
            data.put("op", "publish");
            socket.send(data);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void publish2Hz() {
//            logger.debug("publishing 2Hz");
        //if (hasFlytCloudConnection.get() && mFlightController != null) {
        try {
//                if (!isStateUpdated) {
//                    if (null == aircraftInst || !aircraftInst.isConnected()) {
//                        mFlightController = null;
//                        if (hasDroneConnection.get()) notifyDroneStatusChange();
//                        return;
//                    }
//                }
//                isStateUpdated = false;

//                    setup timestamp
            JSONObject header = new JSONObject();
            header.put("frame_id", "");
            JSONObject stamp = new JSONObject();
            long epochSeconds = System.currentTimeMillis();
            stamp.put("secs", epochSeconds / 1000L);
            stamp.put("nsecs", (epochSeconds % 1000) * 1e6);
            header.put("stamp", stamp);

            //send battery data
            JSONObject msg = new JSONObject();

            msg.put("header", header);

            msg.put("voltage", mVoltage);
            msg.put("capacity", mBatteryCapacity);
            msg.put("current", mCurrent);
            msg.put("charge", mCharge);
            msg.put("percentage", mBatteryPercentage);

            JSONObject data = new JSONObject();
            data.put("topic", "/flytos/mavros/battery");
            data.put("msg", msg);
            data.put("op", "publish");
            socket.send(data);

            //send Vehicle state Data
            JSONObject msg2 = new JSONObject();
            msg2.put("mav_type", 2);
            msg2.put("connected", true);
            if (isFlying) {
                msg2.put("mav_sys_status", 4);
            } else {
                msg2.put("mav_sys_status", 3);
            }

            msg2.put("armed", areMotorsOn);

            msg2.put("guided", isGuided);
            msg2.put("mav_autopilot", 51);
            msg2.put("mode", mode);

            msg2.put("header", header);

            JSONObject data2 = new JSONObject();
            data2.put("topic", "/flytos/flyt/state");
            data2.put("msg", msg2);
            data2.put("op", "publish");
            socket.send(data2);

            //send Home Location data
            JSONObject msg4 = new JSONObject();
            //if (!isNaN(homeLatitude) && !isNaN(homeLongitude) && !isNaN(homeAltitude)) {

            msg4.put("latitude", homeLatitude);
            msg4.put("longitude", homeLongitude);
            msg4.put("altitude", homeAltitude);

            JSONObject data4 = new JSONObject();
            data4.put("topic", "/flytos/mavros/home_position");
            data4.put("msg", msg4);
            data4.put("op", "publish");
            socket.send(data4);
            //}
        } catch (Exception e) {
        }
    }
    //}
}