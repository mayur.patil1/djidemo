package com.mayurflytbase.djidemo;

public interface PlAPICallbacks {
    void sendFailureResponseToServer(String id, String service, String message);
    void sendSuccessResponseToServer(String id, String service, String message);
    void executePL(String id, String service);
    void cancelPL(String id, String service);
    void getConfig(String id, String service);
    void setConfig(String id, String service, int ar_tag_id);
}
