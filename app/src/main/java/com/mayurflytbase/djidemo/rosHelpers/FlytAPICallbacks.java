package com.mayurflytbase.djidemo.rosHelpers;

import org.json.JSONObject;

import edu.wpi.rail.jrosbridge.Service;
import edu.wpi.rail.jrosbridge.services.ServiceRequest;

public interface FlytAPICallbacks {
    void sendFailureResponseToServer(String id, String service, String message);
    void sendSuccessResponseToServer(String id, String service, String message);

    void replyNamespace(String id, String service);

    void land(String id, String service, boolean async);
    void takeOff(String id, String service, double takeOffAlt);

    void velocitySet(String id, String service, float x, float y, float z, float yaw_rate, boolean yaw_rate_valid, boolean isRelative, boolean isBodyFrame);

    void waypointSet(String id, String service, JSONObject args);
    void waypointExecute(String id, String service);
    void waypointGet(String id, String service);
    void waypointClear(String id, String service);
    void waypointPause(String id, String service);
}
