package com.mayurflytbase.djidemo.rosHelpers;

import android.util.Log;

import edu.wpi.rail.jrosbridge.Ros;
import edu.wpi.rail.jrosbridge.Service;
import edu.wpi.rail.jrosbridge.Topic;
import edu.wpi.rail.jrosbridge.callback.CallServiceCallback;
import edu.wpi.rail.jrosbridge.services.ServiceRequest;

public class RosLib {
    static public Ros ros;

    public RosLib() {
    }

    //    Cockpit Topics
    static public Topic battery_pub = new Topic(), state_pub = new Topic(), gimbal_pub = new Topic();
    static public Topic lpos_pub = new Topic(), gpos_pub = new Topic(), imu_pub = new Topic();
    static public Topic obs_pub = new Topic(), alt_pub = new Topic();
    static public Topic flight_stat_pub = new Topic();

    //    Cockpit Services
    static private Service land_srv, takeoff_srv, velocity_set_srv, gpos_set_srv, lpos_set_srv, pos_hold_srv;
    static private Service wp_get_srv, wp_set_srv, wp_clear_srv, wp_execute_srv, wp_pause_srv;
    static private Service rtl_srv, gimbal_set_srv, gimbal_set_v2_srv, ping_srv;
    static private Service camera_configure_srv, camera_capture_srv;
    static private Service setup_sim_srv, restart_app_srv;

    enum RosDashServiceItem {
        LAND("core_api/Land", "/flytos/navigation/land", land_srv),
        TAKE_OFF("core_api/TakeOff", "/flytos/navigation/take_off", takeoff_srv);
        /*VELOCITY_SET("core_api/VelocitySet", "/flytos/navigation/velocity_set", velocity_set_srv),
        POSITION_SET_GLOBAL("core_api/PositionSetGlobal", "/flytos/navigation/position_set_global", gpos_set_srv),
        POSITION_SET("core_api/PositionSet", "/flytos/navigation/position_set", lpos_set_srv),
        POSITION_HOLD("core_api/PositionHold", "/flytos/navigation/position_hold", pos_hold_srv),
        WAYPOINT_GET("core_api/WaypointGet", "/flytos/navigation/waypoint_get", wp_get_srv),
        WAYPOINT_SET("core_api/WaypointSet", "/flytos/navigation/waypoint_set", wp_set_srv),
        WAYPOINT_EXECUTE("core_api/WaypointExecute", "/flytos/navigation/waypoint_execute", wp_execute_srv),
        WAYPOINT_CLEAR("core_api/WaypointClear", "/flytos/navigation/waypoint_clear", wp_clear_srv),
        WAYPOINT_PAUSE("core_api/WaypointPause", "/flytos/navigation/waypoint_pause", wp_pause_srv),
        RTL("core_api/RTL", "/flytos/navigation/rtl", rtl_srv),
        GIMBAL_SET("core_api/GimbalSet", "/flytos/payload/gimbal_set", gimbal_set_srv),
        GIMBAL_SET_V2("flytware_msgs/GimbalSet", "/flytos/dji_msdk/gimbal_set", gimbal_set_v2_srv),
        CAMERA_CONFIGURE("warehouse_demo/CameraConfigure", "/flytos/payload/camera_configure", camera_configure_srv),
        CAMERA_CAPTURE("warehouse_demo/CameraCapture", "/flytos/payload/camera_capture", camera_capture_srv),
        SETUP_SIM("warehouse_demo/CameraCapture", "/flytos/setup/setup_sim", setup_sim_srv),
        RESTART_APP("std_srvs/Trigger", "/flytos/dji_msdk/restart_app", restart_app_srv),
        PING("std_srvs/Empty", "/flytos/mobile/ping", ping_srv);*/

        private final String type;
        private final String name;
        private Service srv_handle;

        RosDashServiceItem(String type, String name, Service srv_handle) {
            this.type = type;
            this.name = name;
            this.srv_handle = srv_handle;
        }
    }

    enum RosDashTopicItem {
        DATA_EULER("geometry_msgs/TwistStamped", "/flytos/mavros/imu/data_euler", imu_pub),
        BATTERY("sensor_msgs/BatteryState", "/flytos/mavros/battery", battery_pub),
        GLOBAL_POSITION("sensor_msgs/NavSatFix", "/flytos/mavros/global_position/global", gpos_pub),
        LOCAL_POSITION("geometry_msgs/TwistStamped", "/flytos/mavros/local_position/local", lpos_pub),
        STATE("mavros_msgs/State", "/flytos/flyt/state", state_pub),
        GIMBAL("geometry_msgs/TwistStamped", "/flytos/mavros/gimbal", gimbal_pub),
        OBS_DISTANCE("warehouse_demo/ObstacleData", "/flytos/dji_msdk/obstacle_distance", obs_pub),
        FLIGHT_STAT("std_msgs/Int32", "/flytos/dji_msdk/flight_stat", flight_stat_pub),
        ALTITUDE("mavros_msgs/Altitude", "/flytos/mavros/altitude", alt_pub);

        private final String type;
        private final String name;
        private Topic topic_handle;

        RosDashTopicItem(String type, String name, Topic topic_handle) {
            this.type = type;
            this.name = name;
            this.topic_handle = topic_handle;
        }
    }

    //    Public Functions Begin
    public void advertiseDashTopics() {
        Log.d("RosLib", "advertiseDashTopics: ");
        for (RosDashTopicItem item : RosDashTopicItem.values()) {
            Log.d("RosLib", "advertiseDashTopics: "+item.name);
            item.topic_handle.init(ros, item.name, item.type);
            item.topic_handle.advertise();
        }
    }

    public void advertiseDashboardServices(FlytAPICallbacks cbF) {
        for (RosDashServiceItem item : RosDashServiceItem.values()) {
            registerDashboardSrvCallbacks(item, cbF);
        }
    }

    //    Public Functions End

    //    Private Functions Begin

    private void registerDashboardSrvCallbacks(RosDashServiceItem item, FlytAPICallbacks cbF) {
        item.srv_handle = new Service(ros, item.name, item.type);
        item.srv_handle.advertiseService(new CallServiceCallback() {
            @Override
            public void handleServiceCall(ServiceRequest request) {
                /*switch (item) {
                    case LAND:
                        cbF.land(request, item.srv_handle);
                        break;
                    case TAKE_OFF:
                        cbF.takeOff(request, item.srv_handle);
                        break;
                }*/
            }
        });
    }


    private void unAdvertiseTopics() {
        for (RosDashTopicItem item : RosDashTopicItem.values()) {
            if (item.topic_handle.isAdvertised()) {
                item.topic_handle.unadvertise();
            }
        }
    }

    private void unAdvertiseServices() {
        for (RosDashServiceItem item : RosDashServiceItem.values()) {
            if (item.srv_handle.isAdvertised()) {
                item.srv_handle.unadvertiseService();
            }
        }
    }

}
