package com.mayurflytbase.djidemo;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.secneo.sdk.Helper;

import java.nio.charset.StandardCharsets;
import java.util.Formatter;

public class MainApplication extends Application {
    private static MainApplication app = null;

    @Override
    protected void attachBaseContext(Context paramContext) {
        super.attachBaseContext(paramContext);
        MultiDex.install(this);
        Helper.install(this);
        app = this;
    }

    public static double[] calculateDistance(double latr, double lonr, double latt, double lont) {
        double[] xy = new double[2];
        if (latr == latt && lonr == lont) {
            return new double[]{0, 0};
        }
        double lat_rad = (latt * Math.PI) / 180.0;
        double lon_rad = (lont * Math.PI) / 180.0;
        double ref_lat_rad = (latr * Math.PI) / 180.0;
        double ref_lon_rad = (lonr * Math.PI) / 180.0;
        double sin_lat = Math.sin(lat_rad);
        double cos_lat = Math.cos(lat_rad);
        double cos_d_lon = Math.cos(lon_rad - ref_lon_rad);
        double ref_sin_lat = Math.sin(ref_lat_rad);
        double ref_cos_lat = Math.cos(ref_lat_rad);
        double c = Math.acos(ref_sin_lat * sin_lat + ref_cos_lat * cos_lat * cos_d_lon);
        double k = Math.abs(c) < Math.ulp(c) ? 1.0 : c / Math.sin(c);
        double x = k * (ref_cos_lat * sin_lat - ref_sin_lat * cos_lat * cos_d_lon) * 6371000;
        double y = k * cos_lat * Math.sin(lon_rad - ref_lon_rad) * 6371000;
        xy[0] = x;
        xy[1] = y;
        return xy;
    }

    public static String getMyFlytbasePayloadHash(String guid, long ts, String serialNumber) {
        String payload = null;
        try {
            String temp = "id=" + guid + "&ts=" + ts + "&key=" + serialNumber + "-n@v5t112R0x";
            java.security.MessageDigest digest = java.security.MessageDigest.getInstance("SHA-1");
            digest.reset();
            digest.update(temp.getBytes(StandardCharsets.UTF_8));
            payload = MainApplication.byteToHex(digest.digest());
        } catch (Exception e) {
            //showLogMessage("Exception in Payload Generation");
        }
        return payload;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
