package com.mayurflytbase.djidemo;

import com.mayurflytbase.djidemo.rosHelpers.FlytAPICallbacks;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

public class JSONParser {
    private static Boolean callService(JSONObject jsonObject, FlytAPICallbacks flytAPICallbacks) throws JSONException {
        JSONObject args = null;
        if (jsonObject.has(KEYS.ARGS.text)) {
            args = jsonObject.getJSONObject(KEYS.ARGS.text);
        }
        String service = jsonObject.getString(KEYS.SERVICE.text);
        String id = jsonObject.getString(KEYS.ID.text);
        //        String[] segments = service.split("/");
//        String command = (segments.length == 1)? segments[segments.length - 1] : segments[segments.length - 2] + "/" + segments[segments.length - 1];
        try {
            if (service.equalsIgnoreCase(VALUES.GET_GLOBAL_NAMSPACE.text)) {
                flytAPICallbacks.replyNamespace(id, service);
            } else if (service.equalsIgnoreCase(VALUES.TAKE_OFF.text)) {
                /*Take Off Altitude Tag**/
                double takeOffAlt = args.getDouble(KEYS.TAKE_OFF_ALT.text);
                //Log.v(TAG, "takeoff_alt Exist- " + takeOffAlt);
                flytAPICallbacks.takeOff(id, service, takeOffAlt);
            } else if (service.equalsIgnoreCase(VALUES.LAND.text)) {
                /*Async Tag**/
                boolean async = args.getBoolean(KEYS.ASYNC.text);
                flytAPICallbacks.land(id, service, async);
            } else if (service.equalsIgnoreCase(VALUES.VELOCITY_SET.text)) {
                /*boolean relative = args.getBoolean(KEYS.RELATIVE.text);
                boolean bodyFrame = args.getBoolean(KEYS.BODY_FRAME.text);
                if (relative && bodyFrame) {
                    relative = false;
                }*/
                float vx = BigDecimal.valueOf(args.getDouble(KEYS.VX.text)).floatValue();
                float vy = BigDecimal.valueOf(args.getDouble(KEYS.VY.text)).floatValue();
                float vz = BigDecimal.valueOf(args.getDouble(KEYS.VZ.text)).floatValue();
                float yawRate = BigDecimal.valueOf(args.getDouble(KEYS.YAW_RATE.text)).floatValue();
                boolean yaw_rate_valid = false;
                if (yawRate != 0.00) {
                    yaw_rate_valid = true;
                }
                //boolean yaw_rate_valid = args.getBoolean(KEYS.YAW_RATE_VALID.text);
                //flytAPICallbacks.velocitySet(id, service, vx, vy, vz, yawRate, yaw_rate_valid, relative, bodyFrame);
                flytAPICallbacks.velocitySet(id, service, vx, vy, vz, yawRate, yaw_rate_valid, false, true);
            } else if (service.equalsIgnoreCase(VALUES.WAYPOINT_SET.text)) {
                /*Waypoint SET**/
                flytAPICallbacks.waypointSet(id, service, args);
            } else if (service.equalsIgnoreCase(VALUES.WAYPOINT_EXECUTE.text)) {
                /*Waypoint Execute**/
                flytAPICallbacks.waypointExecute(id, service);
            } else if (service.equalsIgnoreCase(VALUES.WAYPOINT_PAUSE.text)) {
                /*Waypoint Pause**/
                flytAPICallbacks.waypointPause(id, service);
            } else if (service.equalsIgnoreCase(VALUES.WAYPOINT_GET.text)) {
                /*Waypoint Get**/
                flytAPICallbacks.waypointClear(id, service);
            } else if (service.equalsIgnoreCase(VALUES.WAYPOINT_CLEAR.text)) {
                //Waypoint Cleart*
                flytAPICallbacks.waypointClear(id, service);
            }else {
                return false;
            }
            return true;

        } catch (Exception e) {
            //Rejected Command Responses
            flytAPICallbacks.sendFailureResponseToServer(id, service, VALUES.COMMAND_REJECTED.text + " " + e.getMessage());
            return false;
        }
    }

    private static Boolean callService(JSONObject jsonObject, PlAPICallbacks plAPICallbacks) throws JSONException {
        JSONObject args = null;
        if (jsonObject.has(KEYS.ARGS.text)) {
            args = jsonObject.getJSONObject(KEYS.ARGS.text);
        }
        String service = jsonObject.getString(KEYS.SERVICE.text);
        String id = jsonObject.getString(KEYS.ID.text);
        //        String[] segments = service.split("/");
//        String command = segments[segments.length - 1];
        try {
            if (service.equalsIgnoreCase(VALUES.EXECUTE_PL.text)) {
                plAPICallbacks.executePL(id, service);
            } else if (service.equalsIgnoreCase(VALUES.CANCEL_PL.text)) {
                plAPICallbacks.cancelPL(id, service);
            } else if (service.equalsIgnoreCase(VALUES.SET_PL_CONFIG.text)) {
                int ar_tag_id = args.getInt(KEYS.ARUCO_TAG_ID.text);
                plAPICallbacks.setConfig(id, service, ar_tag_id);
            } else if (service.equalsIgnoreCase(VALUES.GET_PL_CONFIG.text)) {
                plAPICallbacks.getConfig(id, service);
            } else {
                return false;
            }
            return true;
        } catch (Exception e) {
            //Rejected Command Responses
            plAPICallbacks.sendFailureResponseToServer(id, service, VALUES.COMMAND_REJECTED.text + " " + e.getMessage());
            return false;
        }
    }

    static Boolean parseMessage(String data, FlytAPICallbacks flytAPICallbacks) {
        if (data == null) return true;
        try {
            JSONObject jsonObject = new JSONObject(data);
            /*OP Tag**/
            if (isExist(jsonObject, KEYS.OP.text)) {
                String op = jsonObject.getString(KEYS.OP.toString());
                if (op.equalsIgnoreCase(VALUES.CALL_SERVICE.text)) {
                    return callService(jsonObject, flytAPICallbacks);
                }
            }
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    static Boolean parseMessage(String data, PlAPICallbacks plAPICallbacks) {
        if (data == null) return true;
        try {
            JSONObject jsonObject = new JSONObject(data);
            /*OP Tag**/
            if (isExist(jsonObject, KEYS.OP.text)) {
                String op = jsonObject.getString(KEYS.OP.toString());
                if (op.equalsIgnoreCase(VALUES.CALL_SERVICE.text)) {
                    return callService(jsonObject, plAPICallbacks);
                }
            }
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    enum VALUES {
        CALL_SERVICE("call_service"),
        BATTERY("/flytos/mavros/battery"),
        DATA_EULER("/flytos/mavros/imu/data_euler"),
        GLOBAL("/flytos/mavros/global_position/global"),
        LOCAL("/flytos/mavros/local_position/local"),
        STATE("/flytos/flyt/state"),
        GET_GLOBAL_NAMSPACE("/get_global_namespace"),
        TAKE_OFF("/flytos/navigation/take_off"),
        LAND("/flytos/navigation/land"),
        POSITION_HOLD("/flytos/navigation/position_hold"),
        VELOCITY_SET("/flytos/navigation/velocity_set"),
        POSITION_SET("/flytos/navigation/position_set"),
        POSITION_SET_GLOBAL("/flytos/navigation/position_set_global"),
        RTL("/flytos/navigation/rtl"),
        WAYPOINT_SET("/flytos/navigation/waypoint_set"),
        WAYPOINT_EXECUTE("/flytos/navigation/waypoint_execute"),
        WAYPOINT_GET("/flytos/navigation/waypoint_get"),
        WAYPOINT_CLEAR("/flytos/navigation/waypoint_clear"),
        WAYPOINT_PAUSE("/flytos/navigation/waypoint_pause"),
        GET_VIDEO_FEED("/flytos/video_feed/get_type"),
        SET_VIDEO_FEED("/flytos/video_feed/set_type"),
        SET_GPIO("/flytos/diab/io_set"),
        EXECUTE_PL("/flytos/precision_landing/execute"),
        CANCEL_PL("/flytos/precision_landing/cancel"),
        SET_PL_CONFIG("/flytos/precision_landing/set_config"),
        GET_PL_CONFIG("/flytos/precision_landing/get_config"),
        SET_PL_NAME("/flytos/custom/pvds/set_PL_name"),
        GEOFENCE_SET("/flytos/navigation/geofence_set"),
        GEOFENCE_GET("/flytos/navigation/geofence_get"),
        SPOTLIGHT_CONTROL("/flytos/payload/flashlight_control"),
        SPOTLIGHT_STATUS("/flytos/payload/flashlight_status"),
        CAMERA_ZOOM_CONTROL("/flytos/payload/camera/zoom_control"),
        CAMERA_ZOOM_STATUS("/flytos/payload/camera/zoom_status"),
        GET_DUAL_FEED_CONFIG("/flytos/payload/camera/get_dual_feed"),
        SET_DUAL_FEED_CONFIG("/flytos/payload/camera/set_dual_feed"),
        DLB_GET_CRED("/flytos/logger/dlb/get_user_credentials"),
        GIMBAL_SET("/flytos/payload/gimbal_set"),
        SHOOT_PHOTO("shoot_photo"),
        RECORD_VIDEO("record_video"),
        COMMAND_REJECTED("Command Rejected. Bad Request");

        private final String text;

        VALUES(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    enum KEYS {
        OP("op"),
        SERVICE("service"),
        TOPIC("topic"),
        ID("id"),
        ARGS("args"),
        TAKE_OFF_ALT("takeoff_alt"),
        TWIST("twist"),
        LINEAR("linear"),
        X("x"),
        Y("y"),
        Z("z"),
        VX("vx"),
        VY("vy"),
        VZ("vz"),
        YAW_RATE("yaw_rate"),
        YAW_RATE_VALID("yaw_rate_valid"),
        ANGULAR("angular"),
        RELATIVE("relative"),
        BODY_FRAME("body_frame"),
        ASYNC("async"),
        ROLL("roll"),
        PITCH("pitch"),
        YAW("yaw"),
        MODE("mode"),
        TIME("time"),
        INDEX("index"),
        YAW_VALID("yaw_valid"),
        LAT_X("lat_x"),
        LONG_Y("long_y"),
        REL_ALT_Z("rel_alt_z"),
        START("start"),
        GEOFENCE_EN_H("cylEnableHeight"),
        GEOFENCE_EN_R("cylEnableRadius"),
        GEOFENCE_CYL_RADIUS("cylRadius"),
        GEOFENCE_CYL_HEIGHT("cylHeight"),
        GEOFENCE_TYPE("geofenceType"),
        PLName("PL_name"),
        VIDEO_TYPE("video_type"),
        COLOR_SETTING("color_setting"),
        MSX_LEVEL("msx_level"),
        GPIO_ID("id"),
        GPIO_VALUE("value"),
        ARUCO_TAG_ID("ar_tag_id"),
        SPOTLIGHT_ENABLE("enable"),
        SPOTLIGHT_BRIGHTNESS("brightness"),
        ZOOM_LEVEL("zoomlevel"),
        DUAL_FEED_TYPE("dualCameraType"),
        MAIN_CAMERA_BW("mainCameraBw"),
        CAMERA_ID("camera_id");

        private final String text;

        KEYS(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    private static boolean isExist(JSONObject jsonObject, String key) {
        return jsonObject.has(key);
    }
}
